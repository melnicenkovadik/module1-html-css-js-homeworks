function fiba(index) {
    let absIndex = Math.abs(index);
    if (absIndex === 0 || absIndex === 1) {
        return index;
    } else {
        let result = fiba(absIndex - 1) + fiba(absIndex - 2);
        index < 0 ? result = (-1)**(index+1)*result : false;
        return result;
    }
}


while (true) {
    alert(fiba(prompt('Enter index of Fibonacci number')));
}