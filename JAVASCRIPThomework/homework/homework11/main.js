document.addEventListener('DOMContentLoaded', () => {
    // Create array for buttons
    const buttons = {};

    // Select container
    const buttonContainer = document.querySelector('.btn-wrapper');

    // Get all buttons text on the page
    for (let i = 0; i < buttonContainer.children.length; i++) {
        buttons[buttonContainer.children[i].innerText.toLowerCase()] = buttonContainer.children[i];
    }
    console.log(buttons);

    // Function to catch clicks
   document.body.addEventListener('keydown', (element) => {
       if (Object.keys(buttons).includes(element.key.toLowerCase())) {
           if ( document.querySelector('.blue') ) {
               document.querySelector('.blue').className = 'btn';
           }
           buttons[element.key.toLowerCase()].className += ' blue';
       }
   });
});