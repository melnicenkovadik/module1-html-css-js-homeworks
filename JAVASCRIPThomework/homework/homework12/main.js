// Getting all images
const imagesWrapper = document.querySelector('.images-wrapper');
const images = [...imagesWrapper.children].slice(1);

// Styling images container
imagesWrapper.style.position = 'relative';
imagesWrapper.style.boxShadow = '0px 1px 2px 2px';

// Styling images
for (let index in images) {
    images[index].style.position = 'absolute';
    images[index].style.opacity = '0';
    images[index].style.transition = 'opacity .5s';
    images[index].classList.toggle('hidden');
}

// FUNC
function changeToNextImage(imagesWrapper) {
    let nowImage = imagesWrapper.querySelector('img:not(.hidden)');
    let nextImage = nowImage.nextElementSibling !== null ? nowImage.nextElementSibling : imagesWrapper.children[0];
    nowImage.classList.toggle('hidden');
    nowImage.style.opacity = '0';
    nextImage.classList.toggle('hidden');
    nextImage.style.opacity = '1';
}

let sliderPlay = setInterval(changeToNextImage, 2000, imagesWrapper);

// Additional
const buttonContainer = document.createElement('div');
buttonContainer.className = 'buttons';
const stopButton = document.createElement('a');
const playButton = document.createElement('a');
buttonContainer.append(stopButton, playButton);
imagesWrapper.after(buttonContainer);
stopButton.innerText = 'Stop';
playButton.innerText = 'Continue';

buttonContainer.addEventListener('click', (event) => {
    if (event.target === stopButton) {
        clearInterval(sliderPlay);
    } else if (event.target === playButton ) {
        clearInterval(sliderPlay);
        changeToNextImage(imagesWrapper);
        sliderPlay = setInterval(changeToNextImage, 2000, imagesWrapper);
    }
});