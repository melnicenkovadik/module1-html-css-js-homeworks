const container = document.querySelector('.container');
const box = document.querySelector('.box');

function game1() {
    box.addEventListener('mousemove', () => {
        let coords = getRandomCoords(container.clientWidth, container.clientHeight, box.offsetHeight);
        updateBoxCoords(coords);
        changeColor()
    })

    function getRandomCoords(maxX, maxY, boxSize) {
        return {
            x: (maxX - boxSize) * Math.random(),
            y: (maxY - boxSize) * Math.random()
        }
    }

    function updateBoxCoords(coords) {
        box.style.top = `${coords.y}px`;
        box.style.left = `${coords.x}px`
    }

    function changeColor() {
        box.style.borderRadius = `${(Math.random() * 50)}%`;
        // box.style.transform = ``;

        box.style.transform = `scale(${Math.random()}) rotate(${Math.random() * 360}deg)`;
        box.style.backgroundColor = `rgb(${Math.floor(Math.random() * 255)}, ${Math.floor(Math.random() * 255)}, ${Math.floor(Math.random() * 255)})`
        container.style.backgroundColor = `rgba(${Math.floor(Math.random() * 255)}, ${Math.floor(Math.random() * 255)}, ${Math.floor(Math.random() * 255)}, 0.3)`
    }
}

game1();

// function lazyGame() {
//     box.click();
// }

setInterval(lazyGame, 200);

let counter = document.createElement('span');
document.body.append(counter);
let points = 0;
box.addEventListener('mousemove', () => {
    points++
    console.log(points);
    counter.innerText = `POINTS: ${points}`
});
