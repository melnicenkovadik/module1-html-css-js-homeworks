
document.addEventListener('DOMContentLoaded', function () {
    // Get filter HTML element from page
    const priceForm = document.getElementById('filterPrice');

    // Create all needs elements
    // INPUT to get price from user
    const input = document.createElement('input');
    input.placeholder = 'Price';
    input.style.display = 'block';
    input.style.borderColor = 'grey';

    // SPAN to log current price and BUTTON to close
    const currentPrice = document.createElement('span');
    const closeCurrentPrice = document.createElement('button');
    closeCurrentPrice.innerText = 'X';

    // P to log failed price
    const failedPrice = document.createElement('p');
    failedPrice.innerText = `Please enter correct price`;

    // Add all elements to parent element
    priceForm.append(input);

    // FUNCTION
    function inputFocus(element) {
        element.currentTarget.style.borderColor = 'green';
    }
    function inputOutFocus(element) {
        if (element.target.value > 0) {
            element.target.before(currentPrice);
            currentPrice.innerText = `Текущая цена: ${element.target.value}.`;
            currentPrice.after(closeCurrentPrice);
            element.target.style.color = 'green';
            failedPrice.remove();

        } else {
            currentPrice.remove();
            closeCurrentPrice.remove();
            element.target.after(failedPrice);
            element.target.style.borderColor = 'red';
        }
    }
    function removeCurrentPrice () {
        currentPrice.remove();
        closeCurrentPrice.remove();
        input.value = '';
        input.style.borderColor = 'grey';
        input.style.color = 'black'
    }

    // LISTENERS
    input.addEventListener('focus', inputFocus);
    input.addEventListener('blur', inputOutFocus);
    closeCurrentPrice.addEventListener('click', removeCurrentPrice);
});